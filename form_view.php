<?php
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: PUT, GET, POST");
    header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
    header("Content-Type: application/json; charset=UTF-8");

    include "config.php";

    $sql = mysqli_query($conn, "select * from master_barang");
    $barang = array();
    $barang['barang'] = array();
    while ($row = mysqli_fetch_array($sql)){
        $tempData = array(
            "id_barang" => $row['id_barang'],
            "kd_barang" => $row['kd_barang'],
            "nm_barang" => $row['nm_barang'],
            "satuan" => $row['satuan'],
            "harga" => $row['harga']
        );
        array_push($barang['barang'], $tempData);
    }

    echo json_encode($barang);